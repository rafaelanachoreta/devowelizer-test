# Devowelizer Test

Functional test project for Casumo's devowelizer service.

## Prerequisites

Have the devowelizer service container running.

```
docker run -p 8080:8080 -it casumo/devowelizer
```

## Running the tests

To run the tests, just navigate to the root folder of the project and run

```
mvn test
```

By default the tests will expect the devowelizer to be running on http://localhost:8080, but you can change the properties, like so:

```
mvn test -Dhost=http://google.com -Dport=8181
```

## NOTE!
I believe I have found one bug on the service. Please read the comments in the test file for details.