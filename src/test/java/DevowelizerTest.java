import io.restassured.RestAssured;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

/*
    NOTE! Tests were written on the assumption that the occasional 500
    isn't part of the BUSINESS RULES of the service!

    If that were the case, then I would have to implement a mechanism to handle it.
    This could be re-sending the request or even adding a test that checks that on the n-th request a 500 is returned.

    Obviously due to the current way it has been implemented, multiple re-runs will result in different tests failing.
*/
public class DevowelizerTest {
    @BeforeClass
    public static void setup() {
        String port = System.getProperty("port");
        String host = System.getProperty("host");

        RestAssured.port = Integer.valueOf(port);
        RestAssured.baseURI = host;
    }

    @Test
    public void basePathShouldShowInstructions() {
        given().when()
                .get("").then()
                .body(equalTo("Send GET to /:input"))
                .statusCode(200);
    }

    @Test
    public void allVowelsShouldShowEmpty() {
        given().when()
                .get("/aeiou").then()
                .body(equalTo(""))
                .statusCode(200);
    }

    @Test
    public void lowerCaseVowelsShouldBeFiltered() {
        given().when()
                .get("/abcdefghijklmnopqrstuvwxyz1234567890").then()
                .body(equalTo("bcdfghjklmnpqrstvwxyz1234567890"))
                .statusCode(200);
    }

    /*
        NOTE: this is probably a bug on the service!
        I would expect the Devowelizer to devowel both upper and lower case vowels.
        This is something I would check with the team and then file a bug for.
    */
    @Test
    public void upperCaseVowelsShouldBeFiltered() {
        given().when()
                .get("/ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890").then()
                .body(equalTo("BCDFGHJKLMNPQRSTVWXYZ1234567890"))
                .statusCode(200);
    }

    @Test
    public void utf8CharsShouldShow() {
        given().when()
                .get("/%20%21%22%23%24%25%26%27%28%29").then()
                .body(equalTo("%20%21%22%23%24%25%26%27%28%29"))
                .statusCode(200);
    }
}
